# echoes-gitlab-job

## Usage

In your `.gitlab-ci.yml` file:

- add the include `echoes-check.yml`
- add `echoes:check` to the defined `stages`


```yaml
include: https://gitlab.com/echoeshq/echoes-gitlab-job/raw/main/echoes-check.yml

stages:
  - deploy
  - echoes:check

deploy-job:      
  stage: deploy
  script:
    - echo "Application successfully deployed."
```
